"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDbClient = exports.initDevDatabase = exports.getDevDbParams = exports.getMongoDevInstance = void 0;
const mongodb_memory_server_1 = require("mongodb-memory-server");
const mongodb_1 = require("mongodb");
const devmode = process.env.NODE_ENV === 'development';
// cheap lazy launch/di for Mongo memory server
let mongod = void 0;
const getMongoDevInstance = () => {
    if (mongod === void 0) {
        mongod = new mongodb_memory_server_1.MongoMemoryServer();
    }
    return mongod;
};
exports.getMongoDevInstance = getMongoDevInstance;
const getDevDbParams = async (mongo = exports.getMongoDevInstance()) => ({
    uri: await mongo.getUri(),
    port: await mongo.getPort(),
    dbPath: await mongo.getDbPath(),
    dbName: await mongo.getDbName(),
});
exports.getDevDbParams = getDevDbParams;
const initDevDatabase = async () => {
    const mongo = exports.getMongoDevInstance();
    await mongo.ensureInstance();
    return await exports.getDevDbParams(mongo);
};
exports.initDevDatabase = initDevDatabase;
const getDbClient = async () => {
    let uri = '';
    if (devmode) {
        const dbParams = await exports.getDevDbParams(exports.getMongoDevInstance());
        uri = dbParams.uri;
    }
    else if (process.env.DB_URI) {
        uri = process.env.DB_URI;
    }
    const dbClient = new mongodb_1.MongoClient(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    return dbClient;
};
exports.getDbClient = getDbClient;
//# sourceMappingURL=database.js.map