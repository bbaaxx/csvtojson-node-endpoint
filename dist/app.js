"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const providers_1 = require("./resources/providers");
const cars_1 = require("./resources/cars");
const app = express_1.default();
const storage = multer_1.default.memoryStorage();
const uploadMiddleware = multer_1.default({ storage });
app.use(express_1.default.json());
app.get('/ping', async (_, res) => res.status(200).send({ data: 'Pong!' }));
app.get('/providers/:providerName', providers_1.getProvider);
app.post('/providers', providers_1.addProvider);
app.post('/cars', uploadMiddleware.single('upload'), cars_1.addCars);
app.get('/cars', cars_1.getCars);
app.get('/cars/:carId', cars_1.getCar);
exports.default = app;
//# sourceMappingURL=app.js.map