"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProvider = exports.addProvider = void 0;
const database_1 = require("../helpers/database");
const addProvider = async (req, res) => {
    const { providerName, mappings } = req.body;
    if (providerName === '' || providerName === void 0) {
        return res.status(400).send({ data: 'Missing providerName' });
    }
    if (mappings === '' || mappings === void 0) {
        return res.status(400).send({ data: 'Missing provider mappings' });
    }
    const dbClient = await database_1.getDbClient();
    try {
        await dbClient.connect();
        const providers = dbClient
            .db(req.app.get('dbName'))
            .collection('providers');
        const provider = await providers.updateOne({ providerName: providerName }, {
            $setOnInsert: { providerName },
            $set: { mappings },
        }, { upsert: true });
        res.status(200).send({ data: { provider } });
    }
    catch ({ message }) {
        res.status(500).send({ error: message });
    }
    finally {
        await dbClient.close();
    }
};
exports.addProvider = addProvider;
const getProvider = async (req, res) => {
    const { providerName } = req.params;
    const dbClient = await database_1.getDbClient();
    try {
        await dbClient.connect();
        const providers = dbClient
            .db(req.app.get('dbName'))
            .collection('providers');
        const provider = await providers.findOne({ providerName });
        res.status(200).send({ data: { provider } });
    }
    catch ({ message }) {
        res.status(500).send({ error: message });
    }
    finally {
        await dbClient.close();
    }
};
exports.getProvider = getProvider;
//# sourceMappingURL=providers.js.map