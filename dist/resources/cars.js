"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCar = exports.getCars = exports.addCars = void 0;
const csvtojson_1 = __importDefault(require("csvtojson"));
const database_1 = require("../helpers/database");
const safeMappingsFind = (argument, message = 'Provider configuration error, please check mappings') => {
    if (argument === void 0 || argument === null) {
        throw new TypeError(message);
    }
    return argument;
};
const marshalTypes = (targetType, value) => targetType === 'string'
    ? String(value)
    : targetType === 'number'
        ? Number(value)
        : targetType === 'date'
            ? new Date(value)
            : value;
const makeMapper = (mappings) => ([key, value]) => {
    const [_, targetKey, valueType] = safeMappingsFind(mappings.find(([mKey]) => mKey === key));
    const mappedValue = marshalTypes(valueType, value);
    return [targetKey || key, mappedValue];
};
const makeFilter = (mappings) => ([key, value]) => mappings.some(([mapKey]) => mapKey === key);
const makeComplement = (providerName) => (obj) => ({
    ...obj,
    providerName,
});
const addCars = async (req, res) => {
    const { providerName } = req.body;
    const dbClient = await database_1.getDbClient();
    try {
        await dbClient.connect();
        // Get the provider's configuration
        const providers = dbClient
            .db(req.app.get('dbName'))
            .collection('providers');
        const provider = await providers.findOne({ providerName });
        if (!provider)
            throw new Error('Provider configuration not found');
        // get the csv values parsed to JSON
        const { mappings } = provider;
        const csvString = req.file['buffer'].toString();
        const parsedCsv = await csvtojson_1.default({}).fromString(csvString);
        // apply mappings
        const filterObjects = makeFilter(mappings);
        const mapObjects = makeMapper(mappings);
        const complementObjects = makeComplement(providerName);
        const mappedObjects = parsedCsv
            .map(entry => Object.fromEntries(Object.entries(entry).filter(filterObjects).map(mapObjects)))
            .map(complementObjects);
        // save to DB and return results
        const cars = dbClient.db(req.app.get('dbName')).collection('cars');
        const result = await cars.insertMany(mappedObjects);
        res.status(200).send({ data: result });
    }
    catch ({ message }) {
        res.status(500).send({ error: message });
    }
    finally {
        await dbClient.close();
    }
};
exports.addCars = addCars;
const getCars = async (req, res) => {
    const dbClient = await database_1.getDbClient();
    try {
        await dbClient.connect();
        const collection = dbClient.db(req.app.get('dbName')).collection('cars');
        const cars = await collection.find({});
        res.status(200).send({ data: await cars.toArray() });
    }
    catch ({ message }) {
        res.status(500).send({ error: message });
    }
    finally {
        await dbClient.close();
    }
};
exports.getCars = getCars;
const getCar = async (req, res) => {
    const { carId } = req.params;
    const dbClient = await database_1.getDbClient();
    try {
        await dbClient.connect();
        const collection = dbClient.db(req.app.get('dbName')).collection('cars');
        const car = await collection.findOne({ _id: carId });
        res.status(200).send({ data: { car } });
    }
    catch ({ message }) {
        res.status(500).send({ error: message });
    }
    finally {
        await dbClient.close();
    }
};
exports.getCar = getCar;
//# sourceMappingURL=cars.js.map