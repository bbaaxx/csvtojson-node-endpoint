"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDb = exports.getDbClient = exports.initDevDatabase = exports.getMongoDevInstance = void 0;
var mongodb_memory_server_1 = require("mongodb-memory-server");
var mongodb_1 = require("mongodb");
var devmode = process.env.NODE_ENV === 'development';
console.log('devmode', devmode);
// cheap lazy launch/di for Mongo memory server
var mongod = void 0;
var getMongoDevInstance = function () {
    if (mongod === void 0) {
        mongod = new mongodb_memory_server_1.MongoMemoryServer();
    }
    return mongod;
};
exports.getMongoDevInstance = getMongoDevInstance;
var getDevDbParams = function (mongo) {
    if (mongo === void 0) { mongo = exports.getMongoDevInstance(); }
    return __awaiter(void 0, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = {};
                    return [4 /*yield*/, mongo.getUri()];
                case 1:
                    _a.uri = _b.sent();
                    return [4 /*yield*/, mongo.getPort()];
                case 2:
                    _a.port = _b.sent();
                    return [4 /*yield*/, mongo.getDbPath()];
                case 3:
                    _a.dbPath = _b.sent();
                    return [4 /*yield*/, mongo.getDbName()];
                case 4: return [2 /*return*/, (_a.dbName = _b.sent(),
                        _a)];
            }
        });
    });
};
var initDevDatabase = function () { return __awaiter(void 0, void 0, void 0, function () {
    var mongo;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                mongo = exports.getMongoDevInstance();
                return [4 /*yield*/, getDevDbParams(mongo)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
exports.initDevDatabase = initDevDatabase;
// lazy get for the db client
var dbClient = void 0;
var getDbClient = function (uri) { return __awaiter(void 0, void 0, void 0, function () {
    var dbParams;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!(uri === '' && devmode)) return [3 /*break*/, 2];
                return [4 /*yield*/, getDevDbParams(exports.getMongoDevInstance())];
            case 1:
                dbParams = _a.sent();
                uri = dbParams.uri;
                return [3 /*break*/, 3];
            case 2:
                if (process.env.DB_URI) {
                    uri = process.env.DB_URI;
                }
                _a.label = 3;
            case 3:
                if (dbClient === void 0) {
                    dbClient = new mongodb_1.MongoClient(uri, {
                        useNewUrlParser: true,
                        useUnifiedTopology: true,
                    });
                }
                return [2 /*return*/, dbClient];
        }
    });
}); };
exports.getDbClient = getDbClient;
var getDb = function (dbName) { };
exports.getDb = getDb;
//# sourceMappingURL=database.js.map