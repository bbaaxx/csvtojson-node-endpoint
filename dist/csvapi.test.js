"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const app_1 = __importDefault(require("./app"));
describe('Test PingController', () => {
    it('Request /ping should return Pong!', async () => {
        const result = await supertest_1.default(app_1.default).get('/ping').send();
        expect(result.status).toBe(200);
        expect(result.body.data).toBe('Pong!');
    });
});
//# sourceMappingURL=csvapi.test.js.map