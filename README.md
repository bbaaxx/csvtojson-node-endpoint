# CSV Endpoint

This is a simple CSV endpoints that parses CSV to JSON based off a custom mappings
configuration

## Requirements

Based off the requirements described here: https://gist.github.com/JonaMX/6363d975a41c342bf9e13de026704561

Read design considerations on `DESIGN.md`

## How to run

- Clone the repo and cd
- Do a `yarn install` or `npm install`
- For development do `yarn run dev` or `npm run dev`
- For prod start your mongo db and pass `NODE_ENV`, `DB_URI` and `DB_NAME` to your `npm|yarn start`

## API

### POST `/providers`

Create a new provider configuration.
The endpoint expects a JSON body with the following parameters:

- `providerName` : A string that will be the provider Id
- `mappings` : A 2D array of the form `Array[Array[String,String,String]]` containing mappings for property names and data types, the mappings are defined such as: - [0] - Original column name - [1] - Target property name - [2] - Data type

_Returns_: The DB operation result

##### Example request body

    {
      "providerName": "testProvider",
      "mappings": [
        ["UUID", "_id", "string"],
        // ...
        ["Mileage", "milleage", "number"],
        // ...
        ["Update Date", "updateDate", "date"]
      ]
    }

##### CURL example:

    $ curl -X POST -H "Content-Type: application/json" \
    -d @./mocks/createProvider.json <host>:<port>/providers

### GET `/providers/:providerName`

As expected, this will get the provider configuration if a valid `providerName` is provided.

##### CURL example:

    $ curl <host>:<port>/providers/testProvider

### POST `/cars`

Creates car entries from an uploaded CSV file.
The endpoint expects a `'multipart/form-data'` request with the following parameters:

- `providerName` : The name of the provider to fetch the parsing config
- `upload` : The CSV file to process

##### CURL example:

    $ curl -F providerName=testProvider -F upload=@./mocks/cars-codechallenge.csv \
    <host>:<port>/cars

### GET `/cars`

Gets all cars available on the DB
NOTE: As this is not intended for prod, a query result bigger than the heap size could crash the server as the whole cursor is iterated over onto an array. So even if we are talking about several millions of entries here I think the clarification comes in place.

##### CURL example:

    $ curl <host>:<port>/cars

### GET `/cars/:carId`

Returns a car entry if a valid carID is provided.

##### CURL example:

    $ curl <host>:<port>/cars/<valid_car_id>

## Tests

Run tests with `npm|yarn run test`

Currently only integration tests are available.
