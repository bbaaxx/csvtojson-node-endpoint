# Design cosiderations

As requested on the requirements gist, here is a document describing the rationale behind my design decistions.

### Configuration File

According to the requirements I should:

> Build an API with a single endpoint...

However the requirement also states that:

> You are responsible for:
>
> - ...
> - Defining the format of the configuration file and how it is stored/loaded.

I decided to store the configuration for each provider in the database itself so I had to create a second endpoint for this use case.

## Libraries and instrumentation

### Typescript

Selected Typescript as base language because:

- It is pretty awesome not too much for small projects tho but
- It is ok to normalize it's use and
- To show off some TS skills

### Express

Picked express as a backend framework because:

- It has a vibrant community
- Minimal API surface
- Super battle tested

### mongodb-memory-server

Picked mongodb-memory-server as storage because:

- Is fast and simple
- It was suggested on the specs
- I am happy to use mongoDB
- This will not go to production
- End product is JSON not tabular

### MongoDB Native JS driver

Selected the native mongo node driver because:

- No need to deal with schema complexities for only 1 valid static model
- The other data model (actual car entries) can be dealt with with interfaces or otherwise
- We can live with a bit of code duplication/scaffolding

### Multer

Selected multer to parse my multipart request because:

- Seems to be the standard lib these days for multipart in express

### node-csvtojson

Selected node-csvtojson becuase:

- I tested a few others and this one worked pretty well and the code was clear
- The implementation is RFC4180 compliant
- Easy to define custom column parsers (useful with dates but ended up not using this feature in favor of a basic type mapper)

### Jest

- Selected Jest for testing framework because:

  - Is TS friendly
  - Quick to configure
  - Supports helper libaries ootb like supertest (which I also use here)
