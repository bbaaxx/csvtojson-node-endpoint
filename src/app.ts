import express, { Response } from 'express';
import multer from 'multer';

import { getProvider, addProvider } from './resources/providers';
import { addCars, getCars, getCar } from './resources/cars';

const app = express();
const storage = multer.memoryStorage();
const uploadMiddleware = multer({ storage });

app.use(express.json());

app.get('/ping', async (_, res: Response) =>
  res.status(200).send({ data: 'Pong!' }),
);

app.get('/providers/:providerName', getProvider);
app.post('/providers', addProvider);

app.post('/cars', uploadMiddleware.single('upload'), addCars);
app.get('/cars', getCars);
app.get('/cars/:carId', getCar);

export default app;
