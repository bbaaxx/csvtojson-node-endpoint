import { Server } from 'http';

import { getMongoDevInstance, initDevDatabase } from './helpers/database';
import app from './app';

const devmode = process.env.NODE_ENV === 'development';
const servicePort = process.env.PORT || 4200;
const shotdownTimeout = 10000;

// Check required env vars for production/test
if (!devmode) {
  [process.env.DB_URI, process.env.DB_NAME].forEach(val => {
    if (val === void 0 || val === '')
      throw new Error(
        `${[val]} please define DB_URI and DB_NAME env variables`,
      );
  });
}

// Graceful shutdown handler
const shutdown = (server: Server) => async () => {
  console.log('Attempting graceful shutdown');
  // Gracefully stop the dev DB (sole reason for the async/await)
  if (devmode) await getMongoDevInstance().stop();
  server.close(() => {
    console.log('Gracefully closed connections');
    process.exit(0);
  });
  setTimeout(() => {
    console.error('Closing connections timed out, forcing shut down');
    process.exit(1);
  }, shotdownTimeout);
};

const main = async () => {
  // inject the database name
  if (devmode) {
    const { dbName } = await initDevDatabase();
    app.set('dbName', dbName);
  } else {
    app.set('dbName', process.env.DB_NAME);
  }

  const server = app.listen(servicePort);
  const cleanup = shutdown(server);

  // For clarity and to use this 'once' (pun)
  server.once('listening', () =>
    console.log('Listening on port ' + servicePort),
  );

  ['SIGTERM', 'SIGINT'].forEach(signal => process.on(signal, cleanup));

  return server;
};

main().catch(console.error);
