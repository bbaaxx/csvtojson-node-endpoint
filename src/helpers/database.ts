import { MongoMemoryServer } from 'mongodb-memory-server';
import { MongoClient } from 'mongodb';

const devmode = process.env.NODE_ENV === 'development';

export type DevDbParams = {
  uri: string;
  port: number;
  dbPath: string;
  dbName: string;
};

// cheap lazy launch/di for Mongo memory server
let mongod: undefined | MongoMemoryServer = void 0;
export const getMongoDevInstance = () => {
  if (mongod === void 0) {
    mongod = new MongoMemoryServer();
  }
  return mongod;
};

export const getDevDbParams = async (
  mongo: MongoMemoryServer = getMongoDevInstance(),
): Promise<DevDbParams> => ({
  uri: await mongo.getUri(),
  port: await mongo.getPort(),
  dbPath: await mongo.getDbPath(),
  dbName: await mongo.getDbName(),
});

export const initDevDatabase = async (): Promise<DevDbParams> => {
  const mongo = getMongoDevInstance();
  await mongo.ensureInstance();
  return await getDevDbParams(mongo);
};

export const getDbClient = async (): Promise<MongoClient> => {
  let uri: string = '';
  if (devmode) {
    const dbParams = await getDevDbParams(getMongoDevInstance());
    uri = dbParams.uri;
  } else if (process.env.DB_URI) {
    uri = process.env.DB_URI;
  }
  const dbClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  return dbClient;
};
