import { Request, Response } from 'express';
import csv from 'csvtojson';

import { getDbClient } from '../helpers/database';

const safeMappingsFind = <T>(
  argument: T | undefined | null,
  message: string = 'Provider configuration error, please check mappings',
): T => {
  if (argument === void 0 || argument === null) {
    throw new TypeError(message);
  }
  return argument;
};

const marshalTypes = (targetType: string, value: string) =>
  targetType === 'string'
    ? String(value)
    : targetType === 'number'
    ? Number(value)
    : targetType === 'date'
    ? new Date(value)
    : value;

const makeMapper =
  (mappings: Array<[string, string, string]>) =>
  ([key, value]: [key: string, value: any]) => {
    const [_, targetKey, valueType] = safeMappingsFind(
      mappings.find(([mKey]) => mKey === key),
    );
    const mappedValue = marshalTypes(valueType, value);
    return [targetKey || key, mappedValue];
  };

const makeFilter =
  (mappings: Array<[string, string, string]>) =>
  ([key, value]: [key: string, value: any]) =>
    mappings.some(([mapKey]) => mapKey === key);

const makeComplement = (providerName: string) => (obj: { uuid: string }) => ({
  ...obj,
  providerName,
});

export const addCars = async (req: Request, res: Response) => {
  const { providerName } = req.body;
  const dbClient = await getDbClient();
  try {
    await dbClient.connect();

    // Get the provider's configuration
    const providers = dbClient
      .db(req.app.get('dbName'))
      .collection('providers');
    const provider = await providers.findOne({ providerName });
    if (!provider) throw new Error('Provider configuration not found');

    // get the csv values parsed to JSON
    const { mappings } = provider;
    const csvString = req.file['buffer'].toString();
    const parsedCsv = await csv({}).fromString(csvString);

    // apply mappings
    const filterObjects = makeFilter(mappings);
    const mapObjects = makeMapper(mappings);
    const complementObjects = makeComplement(providerName);
    const mappedObjects = parsedCsv
      .map(entry =>
        Object.fromEntries(
          Object.entries(entry).filter(filterObjects).map(mapObjects),
        ),
      )
      .map(complementObjects);

    // save to DB and return results
    const cars = dbClient.db(req.app.get('dbName')).collection('cars');
    const result = await cars.insertMany(mappedObjects);

    res.status(200).send({ data: result });
  } catch ({ message }) {
    res.status(500).send({ error: message });
  } finally {
    await dbClient.close();
  }
};

export const getCars = async (req: Request, res: Response) => {
  const dbClient = await getDbClient();
  try {
    await dbClient.connect();
    const collection = dbClient.db(req.app.get('dbName')).collection('cars');
    const cars = await collection.find({});
    res.status(200).send({ data: await cars.toArray() });
  } catch ({ message }) {
    res.status(500).send({ error: message });
  } finally {
    await dbClient.close();
  }
};

export const getCar = async (req: Request, res: Response) => {
  const { carId } = req.params;
  const dbClient = await getDbClient();
  try {
    await dbClient.connect();
    const collection = dbClient.db(req.app.get('dbName')).collection('cars');
    const car = await collection.findOne({ _id: carId });
    res.status(200).send({ data: { car } });
  } catch ({ message }) {
    res.status(500).send({ error: message });
  } finally {
    await dbClient.close();
  }
};
