import { Request, Response } from 'express';
import { getDbClient } from '../helpers/database';

export const addProvider = async (req: Request, res: Response) => {
  const { providerName, mappings } = req.body;
  if (providerName === '' || providerName === void 0) {
    return res.status(400).send({ data: 'Missing providerName' });
  }
  if (mappings === '' || mappings === void 0) {
    return res.status(400).send({ data: 'Missing provider mappings' });
  }
  const dbClient = await getDbClient();
  try {
    await dbClient.connect();
    const providers = dbClient
      .db(req.app.get('dbName'))
      .collection('providers');
    const provider = await providers.updateOne(
      { providerName: providerName },
      {
        $setOnInsert: { providerName },
        $set: { mappings },
      },
      { upsert: true },
    );
    res.status(200).send({ data: { provider } });
  } catch ({ message }) {
    res.status(500).send({ error: message });
  } finally {
    await dbClient.close();
  }
};

export const getProvider = async (req: Request, res: Response) => {
  const { providerName } = req.params;
  const dbClient = await getDbClient();
  try {
    await dbClient.connect();
    const providers = dbClient
      .db(req.app.get('dbName'))
      .collection('providers');
    const provider = await providers.findOne({ providerName });
    res.status(200).send({ data: { provider } });
  } catch ({ message }) {
    res.status(500).send({ error: message });
  } finally {
    await dbClient.close();
  }
};
