import { readFileSync } from 'fs';
import { resolve } from 'path';
import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';

import app from './app';
import providerConfigMock from '../mocks/createProvider.json';
const csvMockPath = 'mocks/cars-codechallenge.csv';
const validCarId = 'ce1c7d15-c51a-4db5-b3e4-33dc04d96080';

describe('Test Ping Endpoint', () => {
  it('Request /ping should return Pong!', async () => {
    const result = await request(app).get('/ping').send();
    expect(result.status).toBe(200);
    expect(result.body.data).toBe('Pong!');
  });
});

describe('CSV API endpoints', () => {
  let mongod: MongoMemoryServer;
  let csvFile: Buffer;
  beforeAll(async () => {
    csvFile = readFileSync(csvMockPath);
    mongod = new MongoMemoryServer();
    process.env.DB_URI = await mongod.getUri();
    process.env.DB_NAME = await mongod.getDbName();
    app.set('dbName', process.env.DB_NAME);
  });
  afterAll(async () => {
    await mongod.stop();
  });

  describe('POST /providers', () => {
    it('Should create a provider configuration on DB', async () => {
      const result = await request(app)
        .post('/providers')
        .set('Content-Type', 'application/json')
        .send(providerConfigMock);

      expect(result.status).toBe(200);
      const { n, ok } = result.body.data.provider;
      expect(n).toBe(1);
      expect(ok).toBe(1);
    });
  });

  describe('GET /providers/:providerName', () => {
    it('Should get the provider injected previously by name', async () => {
      const result = await request(app).get('/providers/testProvider').send();

      expect(result.status).toBe(200);
      const { providerName } = result.body.data.provider;
      expect(providerName).toBe('testProvider');
    });
  });

  describe('POST /cars', () => {
    it('Should insert car registries for each row on the CSV', async () => {
      const result = await request(app)
        .post('/cars')
        .attach('upload', csvFile, 'csvfile.csv')
        .field('providerName', 'testProvider');

      expect(result.status).toBe(200);
      const { n, ok } = result.body.data.result;
      expect(n).toBe(3);
      expect(ok).toBe(1);
    });
  });

  describe('get /cars', () => {
    it('Should fetch all available cars on DB', async () => {
      const result = await request(app).get('/cars').send();
      expect(result.status).toBe(200);
      const { data } = result.body;
      expect(data.length).toEqual(3);
    });
  });

  describe('get /cars/:carId', () => {
    it('Should fetch a car from DB by id', async () => {
      const result = await request(app).get(`/cars/${validCarId}`).send();
      expect(result.status).toBe(200);
      const { car } = result.body.data;
      expect(car.make).toEqual('Subaru');
    });
  });
});
